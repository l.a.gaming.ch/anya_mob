
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.anyamob.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.common.ForgeSpawnEggItem;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;

import net.mcreator.anyamob.AnyaMobMod;

public class AnyaMobModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, AnyaMobMod.MODID);
	public static final RegistryObject<Item> ANYA = REGISTRY.register("anya_spawn_egg",
			() -> new ForgeSpawnEggItem(AnyaMobModEntities.ANYA, -1, -1, new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
}
